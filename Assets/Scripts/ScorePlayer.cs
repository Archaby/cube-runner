﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePlayer : MonoBehaviour
{
    public Transform player;
    Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();    
    }

    // Update is called once per frame
    void Update()
    {
        int pos = (int)(-(player.position.z));
        scoreText.text = pos.ToString();        
    }
}
