﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    void OnTriggerEnter() {
        FindObjectOfType<GameManager>().EndLevel();
    }
}
