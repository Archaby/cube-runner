﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public AudioClip crush;

    void OnCollisionEnter(Collision col)
    {

        PlayerController controller = GetComponent<PlayerController>();
        AudioSource audioSource = GetComponent<AudioSource>();

        if (col.collider.tag == "Obstacle")
        {

            if (audioSource != null)
            {
                audioSource.Stop();
            }

            if (controller != null)
            {
                controller.PlaySound(crush);
                controller.enabled = false;
            }

            FindObjectOfType<GameManager>().EndGame();
        }

        if (col.collider.tag == "FinishCol")
        {
            audioSource.Stop();
            controller.enabled = false;
            //Debug.Log("Finish");
        }

    }
}
