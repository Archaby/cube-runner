﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject LevelComplete;
    bool isEndGame = false;
    public float sceneDelay = 1f;

    public void EndGame () {
        if (!isEndGame)
        {
            isEndGame = true;
            Invoke("Restart", sceneDelay);
        }
    }

    void Restart () {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void EndLevel() {
        LevelComplete.SetActive(true);
        Debug.Log("End Level");
    }
  
}
