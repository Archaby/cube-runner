﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    AudioSource audioSource;
    Rigidbody rb;
    public float varForwBackSpeed = 1000f;
    public float varLeftRightSpeed = 500f;
    //public AudioClip raceSound;
    // Start is called before the first frame update
    
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        rb = GetComponent <Rigidbody> ();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
        rb.AddForce(0, 0, -varForwBackSpeed * Time.deltaTime);
        //GameObject.PlaySound(raceSound);

        if (Input.GetKey("a"))
        {
            rb.AddForce(varLeftRightSpeed * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (Input.GetKey("d"))
        {
            rb.AddForce(-varLeftRightSpeed * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        if (rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
    
}
